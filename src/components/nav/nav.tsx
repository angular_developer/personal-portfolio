import './nav.css'

const Nav = () => {
    return (
        <>
            <div className="container nav">
                <div className="navbar-brand pt-3 ps-3">
                    <a href="">
                        <img src="../src/assets/s-logo.svg" alt="logo" width={40} />
                    </a>
                </div>
            </div>
        </>
    )
}

export { Nav };