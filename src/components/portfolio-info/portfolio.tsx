import './portfolio.css';

const Portfolio_Info = () => {
    return (
        <>
            <div className='main'>
                <div className='title-text'>
                    <h1>Welcome.</h1>
                    <div className="inner-text">
                        <span></span>
                        <div className='text'>
                            <p className='mb-2'>My name is <span className='candiate-name'>Sakthivel Selvam</span>. I'm a front-end developer located in Erode, Tamilnadu, India. I have Developed a many type of front-end.</p>
                            <p className='m-0'>I am passionate about cutting-edge, pixel-perfet, beautiful interfaces.</p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export { Portfolio_Info };